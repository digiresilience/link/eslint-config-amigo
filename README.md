# eslint-config-amigo

A shared eslint config for [CDR Tech][cdrtech].

# Install

We recommend using [@digiresilience/amigo-dev][amigo-dev] to manage your dev dependencies.

[amigo-dev]: https://gitlab.com/digiresilience/link/amigo-dev

But if you want to do it manually, then:

```console
$ yarn add -D @digiresilience/eslint-config-amigo
```

# Usage

**`.eslintrc.js`**

```js
require('@digiresilience/eslint-config-amigo/patch/modern-module-resolution');
module.exports = {
  extends: [
    // one of:
    "@digiresilience/eslint-config-amigo/profile/browser", // if targeting the browser
    "@digiresilience/eslint-config-amigo/profile/node", // if targeting node

    // and optionally:
    "@digiresilience/eslint-config-amigo/profile/typescript", // if using typescript (node or browser)
    "@digiresilience/eslint-config-amigo/profile/cypress",    // if using cypress
    "@digiresilience/eslint-config-amigo/profile/jest"        // if using jest
  ],
  parserOptions: { tsconfigRootDir: __dirname }
};
```

# Credits

Copyright © 2020-present [Center for Digital Resilience][cdr]

### Contributors

|  [![Abel Luck][abelxluck_avatar]][abelxluck_homepage]<br/>[Abel Luck][abelxluck_homepage] |
|---|

[abelxluck_homepage]: https://gitlab.com/abelxluck
[abelxluck_avatar]: https://secure.gravatar.com/avatar/0f605397e0ead93a68e1be26dc26481a?s=100&amp;d=identicon

### License

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0.en.html)

    GNU AFFERO GENERAL PUBLIC LICENSE
    Version 3, 19 November 2007

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

[cdrtech]: https://digiresilience.org/tech/
[cdr]: https://digiresilience.org
