# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.3.10](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.3.9...0.3.10) (2021-10-11)

### [0.3.9](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.3.8...0.3.9) (2021-10-11)

### [0.3.8](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.3.7...0.3.8) (2021-10-11)

### [0.3.7](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.3.6...0.3.7) (2021-10-08)

### [0.3.6](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.3.4...0.3.6) (2021-05-27)

### [0.3.4](https://gitlab.com/digiresilience/link/eslint-config-amigo/compare/0.3.3...0.3.4) (2021-05-27)

### [0.3.3](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.3.2...0.3.3) (2021-05-27)

### [0.3.2](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.3.1...0.3.2) (2021-05-27)

### [0.3.1](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.3.0...0.3.1) (2021-05-03)


### Bug Fixes

* tweak linter settings ([cf07f51](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/commit/cf07f51c4547336a9bcf44b1e203437e76fe593e))

## [0.3.0](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.2.3...0.3.0) (2021-05-03)


### ⚠ BREAKING CHANGES

* bump deps. new lint rules.

### Features

* bump deps. new lint rules. ([df884f9](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/commit/df884f935fe3742fa8d908cb219193afb9a3017f))

### [0.2.3](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.2.2...0.2.3) (2020-12-02)


### Bug Fixes

* disable unicorn/no-fn-reference-in-iterator as it breaks ramda and ([f719da8](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/commit/f719da8ab9bf8ebc5cec0c243cf7727c4baebc62))

### [0.2.2](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.2.1...0.2.2) (2020-11-24)


### Features

* add cypress profile ([8773fca](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/commit/8773fca27bb88835e099a0d05c0cf5eae2744022))
* make jest optional ([36b7128](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/commit/36b71289b46660d5d0c7a79e93554a51158c5bfd))


### Bug Fixes

* add eslint-plugin-cypress dep ([16e4384](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/commit/16e4384181593ce058f2cbc11b9e206af12ff76c))

### [0.2.1](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.2.0...0.2.1) (2020-11-21)

## [0.2.0](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.1.4...0.2.0) (2020-11-20)


### ⚠ BREAKING CHANGES

* upgrade deps, inlcuding TS to 4.1

### Features

* upgrade deps, inlcuding TS to 4.1 ([f47332e](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/commit/f47332ec0f9dd6fdd28211f561b0109c68e7d3e0))

### [0.1.4](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.1.3...0.1.4) (2020-11-20)

### [0.1.3](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.1.2...0.1.3) (2020-11-10)

### [0.1.2](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.1.1...0.1.2) (2020-11-05)


### Bug Fixes

* Add no-unused-var _ prefix exception ([6d27d3a](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/commit/6d27d3a482ca6a082c06f2034b72dda8b1f1d74d))

### [0.1.1](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/compare/0.1.0...0.1.1) (2020-10-09)


### Bug Fixes

* Fix jest support ([ce49875](https://gitlab.com/digiresilience.org/link/eslint-config-amigo/commit/ce49875c4031db25f782dead55cb3ff1621673a2))

## 0.1.0 (2020-10-09)
