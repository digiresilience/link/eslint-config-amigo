// Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
// See LICENSE at
// https://github.com/microsoft/rushstack/tree/master/stack/eslint-config
require('@rushstack/eslint-patch/modern-module-resolution');
