const path = require("path");
module.exports = {
  extends: ["xo-space", path.join(__dirname, "common.js")],
  env: {
    es2021: true,
    node: true,
  },
  rules: {},
};
