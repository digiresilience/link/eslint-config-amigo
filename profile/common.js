module.exports = {
  // root: true,
  ignorePatterns: ["node_modules", "build", "coverage"],
  plugins: [
    // Plugin documentation: https://www.npmjs.com/package/eslint-plugin-unicorn
    "unicorn",
    // Plugin documentation: https://github.com/dustinspecker/eslint-plugin-no-use-extend-native
    "no-use-extend-native",
    // Plugin documentation: https://github.com/xjamundx/eslint-plugin-promise
    "promise",
    // Plugin documentation: https://github.com/benmosher/eslint-plugin-import
    "import",
    // Plugin documentation: https://github.com/mysticatea/eslint-plugin-eslint-comments
    "eslint-comments",
  ],
  extends: ["plugin:unicorn/recommended", "prettier"],
  env: {},
  rules: {
    // these are handled by prettier
    quotes: "off",
    "object-curly-spacing": "off",
    "comma-dangle": "off",

    // reduce the tyranny
    "capitalized-comments": "off",
    "new-cap": "off",

    // plugin rules
    "no-use-extend-native/no-use-extend-native": "error",
    // this one breaks libraries like Ramda and lodash
    "unicorn/no-array-callback-reference": "off",
    "unicorn/better-regex": [
      "error",
      {
        sortCharacterClasses: false,
      },
    ],
    "unicorn/prevent-abbreviations": "off",
    "unicorn/consistent-function-scoping": "off",
    "unicorn/no-useless-undefined": "off",
    "unicorn/no-array-reduce": "off",
    "unicorn/no-array-for-each": "off",
    "function-call-argument-newline": "off",
    "promise/param-names": "error",
    "promise/no-return-wrap": [
      "error",
      {
        allowReject: true,
      },
    ],
    "promise/no-new-statics": "error",
    "promise/no-return-in-finally": "error",
    "promise/valid-params": "error",
    "promise/prefer-await-to-then": "error",
    "import/default": "error",
    "import/export": "error",
    "import/extensions": [
      "error",
      {
        js: "never",
        jsx: "never",
        json: "always",
        svg: "always",
        css: "always",
        test: "ignorePackages",
      },
    ],

    "import/namespace": [
      "error",
      {
        allowComputed: true,
      },
    ],
    "import/no-absolute-path": "error",
    "import/no-anonymous-default-export": "error",
    "import/no-named-default": "error",
    "import/no-webpack-loader-syntax": "error",
    "import/no-self-import": "error",

    "import/no-useless-path-segments": [
      "error",
      {
        noUselessIndex: true,
      },
    ],
    "import/no-amd": "error",
    "import/no-duplicates": "error",

    // enable this when  this is fixed
    // https://github.com/benmosher/eslint-plugin-import/pull/1696
    "import/no-extraneous-dependencies": "off",

    "import/no-mutable-exports": "error",
    "import/no-named-as-default-member": "error",
    "import/no-named-as-default": "error",
    "eslint-comments/no-aggregating-enable": "error",
    "eslint-comments/no-duplicate-disable": "error",
    "eslint-comments/no-unused-disable": "error",
    "eslint-comments/no-unused-enable": "error",
    "no-prototype-builtins": "off",
    "no-unused-vars": [
      "error",
      {
        argsIgnorePattern: "^_",
      },
    ],
  },
};
