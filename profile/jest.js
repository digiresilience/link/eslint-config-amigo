const path = require("path");
module.exports = {
  extends: ["plugin:jest/recommended"],
  plugins: [
    // Plugin documentation: https://www.npmjs.com/package/eslint-plugin-jest
    "jest",
  ],
  env: {
    "jest/globals": true,
  },
  rules: {
    "jest/valid-expect": "off",
  },
};
