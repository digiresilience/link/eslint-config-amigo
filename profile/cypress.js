const path = require("path");
module.exports = {
  extends: ["plugin:cypress/recommended"],
  plugins: ["cypress"],
  env: {
    "cypress/globals": true,
  },
  rules: {},
};
