module.exports = {
  parser: "@typescript-eslint/parser",
  parserOptions: { project: "./tsconfig*json" },
  plugins: [
    // Plugin documentation: https://www.npmjs.com/package/@typescript-eslint/eslint-plugin
    "@typescript-eslint/eslint-plugin",
  ],
  extends: ["plugin:@typescript-eslint/recommended"],
  rules: {
    "no-useless-constructor": "off",
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": [
      "error",
      {
        argsIgnorePattern: "^_",
      },
    ],
    "no-extra-semi": "off",
    "@typescript-eslint/no-extra-semi": "error",
  },
};
